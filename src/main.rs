// {{{ Imports
#![cfg(not(test))]

use std::env;
use std::collections::HashSet;

use multi_dice::{parse, roll, utils};

use log::{error, info, warn};

use serenity::client::Client;

use serenity::framework::standard::StandardFramework;

use serenity::http::Http;
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::model::id::UserId;
use serenity::prelude::{EventHandler, Context};
use serenity::framework::standard::{
    self,
    Args,
    CommandResult,
    CommandGroup,
    HelpOptions,
    macros::{
        command,
        group,
        help,
    }
};
// }}}

#[group]
#[commands(dice, age)]
#[default_command(dice)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);
    }
}

#[command]
#[description("Primary rolling command")]
#[usage("roll!dice XdY [(+|-) XdY] [(+|-) N] [(keep|drop) K]")]
#[example("roll!dice 1d20 + 1d4 - 1")]
async fn dice(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut results: Vec<String> = vec![];
    for arg in args.iter::<String>() {
        let arg = arg?;
        let result = match parse::dice(&arg) {
            Ok(val) => val,
            Err(e) => {
                warn!("command::standard failed: {}", e);
                msg.reply(ctx, format!("I can't do {} ({})", arg, e)).await?;
                return Ok(()); // I don't think the framework is expecting user errors
            }
        };
        results.push(result.to_string());
    }
    let final_result = utils::reduce(&mut results.iter().map(|r| r.to_string()), |acc, r| acc + "; " + &r);
    msg.reply(ctx, format!("Results: {}", final_result)).await?;
    Ok(())
}

#[command]
#[description("Specialized command for the AGE system")]
#[usage("roll!age")]
async fn age(ctx: &Context, msg: &Message) -> CommandResult {
    let roll1 = roll::single(6);
    let roll2 = roll::single(6);
    let stunt = roll::single(6);
    let total = roll1 + roll2 + stunt;
    msg.reply(ctx, format!("Results: {} + {} + (stunt) {} = {}", roll1, roll2, stunt, total)).await?;
    Ok(())
}

#[help]
async fn help(ctx: &Context, msg: &Message, args: Args, help_options: &'static HelpOptions, groups: &[&'static CommandGroup], owners: HashSet<UserId>) -> CommandResult {
    let _ = standard::help_commands::with_embeds(ctx, msg, args, help_options, groups, owners).await;
    Ok(())
}

// {{{ Main
#[tokio::main]
async fn main() {
    let log_level = env::var("LOG_LEVEL")
        .ok()
        .and_then(level_str_to_enum)
        .unwrap_or(log::LevelFilter::Info);
    setup_logger(log_level).expect("Error setting up logger");
    let token = env::var("DISCORD_TOKEN").expect("Error reading DISCORD_TOKEN");
    let http = Http::new_with_token(&token);
    let (_owners, bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            if let Some(team) = info.team {
                owners.insert(team.owner_user_id);
            } else {
                owners.insert(info.owner.id);
            }
            match http.get_current_user().await {
                Ok(bot_id) => (owners, bot_id.id),
                Err(why) => panic!("Could not access bot id: {:?}", why),
            }
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("roll!")
                   .on_mention(Some(bot_id))
                   .delimiters(vec![", ", ","])
        )
        .help(&HELP)
        .group(&GENERAL_GROUP);
    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Could not create client");

    if let Err(why) = client.start().await {
        error!("An error occurred: {:?}", why);
    }
}

fn level_str_to_enum(input: String) -> Option<log::LevelFilter> {
    match input.to_lowercase().as_ref() {
        "trace" => Some(log::LevelFilter::Trace),
        "debug" => Some(log::LevelFilter::Debug),
        "info"  => Some(log::LevelFilter::Info),
        "warn"  => Some(log::LevelFilter::Warn),
        "error" => Some(log::LevelFilter::Error),
        _ => None,
    }
}

fn setup_logger(level: log::LevelFilter) -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "({}) {} {} {}",
                record.level(),
                chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
                record.target(),
                message
            ))
        })
        .level(level)
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}
//}}}
