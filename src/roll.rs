use rand::distributions::{Distribution, Uniform};

pub fn between(lower: isize, upper: isize) -> isize {
    let dist = Uniform::from(lower..=upper);
    let mut rng = rand::thread_rng();
    dist.sample(&mut rng)
}

pub fn single(sides: isize) -> isize {
    between(1, sides)
}

pub fn standard(n: isize, sides: isize) -> Vec<isize> {
    let mut out = vec![];
    let between = Uniform::from(1..=sides);
    let mut rng = rand::thread_rng();
    for _ in 0..n {
        out.push(between.sample(&mut rng));
    }
    out
}

#[cfg(test)]
mod tests {
}