pub trait Tokenizer<'a> {
    type TokenIter: Iterator<Item = Token>;

    fn tokenize(&self, input: &'a str) -> Self::TokenIter;
}

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
    Plus,
    Minus,
    Keep,
    Cut,
    Above,
    Below,
    Fate,
    Boost,
    Ability,
    Proficiency,
    Setback,
    Difficulty,
    Challenge,
    Total(String),
    Number(String),
}

pub struct DiceTokenIter<'a> {
    input: &'a str,
    byte_offset: usize,
    char_offset: usize,
    position: usize,
}

impl<'a> DiceTokenIter<'a> {
    pub fn new(input: &'a str) -> Self {
        Self { input, byte_offset: 0, char_offset: 0, position: 0 }
    }

    fn increment_offsets(&mut self, c: char) {
        self.byte_offset += c.len_utf8();
        self.char_offset += 1;
        self.position += 1;
    }
}

impl<'a> Iterator for DiceTokenIter<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        for c in self.input[self.byte_offset..].chars() {
            match c {
                'f' | 'F' => {
                    self.increment_offsets(c);
                    return Some(Token::Fate);
                },
                'b' | 'B' => {
                    let tok: String = self.input[self.byte_offset..].chars().take_while(|c| c.is_alphabetic()).collect();
                    if tok.to_lowercase() == "below" {
                        self.byte_offset += tok.bytes().count();
                        self.char_offset += tok.chars().count();
                        self.position += 1;
                        return Some(Token::Below);
                    } else {
                        self.increment_offsets(c);
                        return Some(Token::Boost);
                    }
                },
                'a' | 'A' => {
                    let tok: String = self.input[self.byte_offset..].chars().take_while(|c| c.is_alphabetic()).collect();
                    if tok.to_lowercase() == "above" {
                        self.byte_offset += tok.bytes().count();
                        self.char_offset += tok.chars().count();
                        self.position += 1;
                        return Some(Token::Above);
                    } else {
                        self.increment_offsets(c);
                        return Some(Token::Ability);
                    }
                },
                'p' | 'P' => {
                    self.increment_offsets(c);
                    return Some(Token::Proficiency);
                },
                's' | 'S' => {
                    self.increment_offsets(c);
                    return Some(Token::Setback);
                },
                'd' | 'D' => {
                    self.increment_offsets(c);
                    return Some(Token::Difficulty);
                },
                'c' | 'C' => {
                    let tok: String = self.input[self.byte_offset..].chars().take_while(|c| c.is_alphabetic()).collect();
                    if tok.to_lowercase() == "cut" {
                        self.byte_offset += tok.bytes().count();
                        self.char_offset += tok.chars().count();
                        self.position += 1;
                        return Some(Token::Cut);
                    } else {
                        self.increment_offsets(c);
                        return Some(Token::Challenge);
                    }
                }
                'k' | 'K' => {
                    let tok: String = self.input[self.byte_offset..].chars().take_while(|c| c.is_alphabetic()).collect();
                    if tok.to_lowercase() == "keep" {
                        self.byte_offset += tok.bytes().count();
                        self.char_offset += tok.chars().count();
                        self.position += 1;
                        return Some(Token::Keep);
                    } else {
                        break;
                    }
                }
                '+' => {
                    self.increment_offsets(c);
                    return Some(Token::Plus);
                },
                '-' => {
                    self.increment_offsets(c);
                    return Some(Token::Minus);
                },
                '0'..='9' => {
                    let tok: String = self.input[self.byte_offset..].chars().take_while(|c| c.is_digit(10)).collect();
                    self.byte_offset += tok.bytes().count();
                    self.char_offset += tok.chars().count();
                    self.position += 1;
                    if self.input[self.byte_offset..].starts_with(|c| c == 'd' || c == 'D') {
                        self.byte_offset += 1;
                        self.char_offset += 1;
                        return Some(Token::Total(tok));
                    } else {
                        return Some(Token::Number(tok));
                    }
                },
                _ if c.is_whitespace() => {
                    self.increment_offsets(c);
                    continue
                },
                _ => break,
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tokenizer_recognizes_all_keywords() {
        let input = "+-";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Minus));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_recognizes_2d4() {
        let input = "2d4";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("2"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("4"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_recognizes_100d12() {
        let input = "100d12";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("100"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("12"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_recognizes_complete_expression() {
        let input = "3d12+5";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("3"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("12"))));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Number(String::from("5"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_skips_whitespace() {
        let input = "4d20 + 6";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("20"))));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_handle_multiple_dice_sets() {
        let input = "4d30 + 6d6 + 8d4";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("30"))));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("8"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("4"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_recognizes_dice_regardless_of_case() {
        let input = "10d4 + 12D6";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("10"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("12"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_above() {
        let input = "4d6 above 3";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Above));
        assert_eq!(tok.next(), Some(Token::Number(String::from("3"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_below() {
        let input = "4d6 below 3";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Below));
        assert_eq!(tok.next(), Some(Token::Number(String::from("3"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_cut() {
        let input = "4d6 cut 3";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Cut));
        assert_eq!(tok.next(), Some(Token::Number(String::from("3"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_keep() {
        let input = "4d6 keep 3";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Number(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Keep));
        assert_eq!(tok.next(), Some(Token::Number(String::from("3"))));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_fate() {
        let input = "3dF";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("3"))));
        assert_eq!(tok.next(), Some(Token::Fate));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_read_genesys() {
        let input = "1dB + 2dA + 3dP + 4dS + 5dD + 6dC";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("1"))));
        assert_eq!(tok.next(), Some(Token::Boost));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("2"))));
        assert_eq!(tok.next(), Some(Token::Ability));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("3"))));
        assert_eq!(tok.next(), Some(Token::Proficiency));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Setback));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("5"))));
        assert_eq!(tok.next(), Some(Token::Difficulty));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Challenge));
        assert_eq!(tok.next(), None);
    }

    #[test]
    fn tokenizer_can_recognize_special_dice_regardless_of_case() {
        let input = "10Df + 1Db + 2Da + 3Dp + 4Ds + 5Dd + 6Dc";
        let mut tok = DiceTokenIter::new(input);
        assert_eq!(tok.next(), Some(Token::Total(String::from("10"))));
        assert_eq!(tok.next(), Some(Token::Fate));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("1"))));
        assert_eq!(tok.next(), Some(Token::Boost));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("2"))));
        assert_eq!(tok.next(), Some(Token::Ability));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("3"))));
        assert_eq!(tok.next(), Some(Token::Proficiency));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("4"))));
        assert_eq!(tok.next(), Some(Token::Setback));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("5"))));
        assert_eq!(tok.next(), Some(Token::Difficulty));
        assert_eq!(tok.next(), Some(Token::Plus));
        assert_eq!(tok.next(), Some(Token::Total(String::from("6"))));
        assert_eq!(tok.next(), Some(Token::Challenge));
        assert_eq!(tok.next(), None);
    }
}
