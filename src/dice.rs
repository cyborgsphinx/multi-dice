use std::fmt;
use std::cmp::Ordering;

use super::roll;
use super::utils;

/// A trait for getting a random roll from an object
/// Not guaranteed to have the same value every call
pub trait Roll<T> {
    fn roll(&self) -> T;
}

#[derive(Debug, PartialEq, Clone)]
pub struct NumericDice {
    total: isize,
    sides: isize,
    is_positive: bool,
}

impl<'a> NumericDice {
    pub fn new(total: isize, sides: isize) -> Self {
        Self {
            total,
            sides,
            is_positive: true,
        }
    }

    pub fn negate(self) -> Self {
        Self {
            is_positive: !self.is_positive,
            ..self
        }
    }

    fn expr(&self) -> String {
        format!("{}d{}", self.total, self.sides)
    }
}

impl Roll<(isize, Vec<isize>)> for NumericDice {
    fn roll(&self) -> (isize, Vec<isize>) {
        let history = roll::standard(self.total, self.sides);
        let value = history.iter().sum::<isize>();
        (value * if self.is_positive { 1 } else { -1 }, history)
    }
}

impl fmt::Display for NumericDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (roll, history) = self.roll();
        if self.is_positive {
            write!(f, "({}d{} -> {:?}): {}", self.total, self.sides, history, roll)
        } else {
            write!(f, "(-{}d{} -> {:?}): {}", self.total, self.sides, history, roll)
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct PlainDice {
    dice: Vec<NumericDice>,
    scalar: isize,
}

impl PlainDice {
    pub fn new(dice: Vec<NumericDice>, scalar: isize) -> Self {
        Self { dice, scalar, }
    }

    pub fn with_dice(dice: Vec<NumericDice>) -> Self {
        Self { dice, scalar: 0, }
    }

    pub fn add_dice(&mut self, dice: NumericDice) {
        self.dice.push(dice);
    }

    pub fn update_scalar(&mut self, scalar: isize) {
        self.scalar += scalar;
    }
}

impl Roll<isize> for PlainDice {
    fn roll(&self) -> isize {
        let mut value = self.scalar;
        for dice in &self.dice {
            let (val, _) = dice.roll();
            value += val;
        }
        value
    }
}

impl fmt::Display for PlainDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if !self.dice.is_empty() {
            let s = utils::reduce(
                &mut self.dice.iter().map(|d| format!("({} -> {:?})", d.expr(), d.roll().1)),
                |acc, d| format!("{} + {}", acc, d));
            write!(f, "{} ", s)?;
        }
        match self.scalar.cmp(&0) {
            Ordering::Greater => write!(f, "+ {}",  self.scalar),
            Ordering::Less    => write!(f, "- {}", -self.scalar),
            Ordering::Equal   => Ok(()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct TrimDice {
    dice: NumericDice,
    to_keep: isize,
    keep_higher: bool,
}

impl<'a> TrimDice {
    pub fn new(dice: NumericDice, to_keep: isize, keep_higher: bool) -> Self {
        Self {
            dice,
            to_keep,
            keep_higher,
        }
    }
}

impl Roll<isize> for TrimDice {
    fn roll(&self) -> isize {
        let (_, history) = self.dice.roll();
        let to_keep = if self.to_keep > 0 { self.to_keep as usize } else { 0 };
        let value = if self.keep_higher {
            let to_drop = history.len() - to_keep;
            history.iter().skip(to_drop).sum::<isize>()
        } else {
            history.iter().take(to_keep).sum::<isize>()
        };
        value
    }
}

impl fmt::Display for TrimDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let op = if self.keep_higher { "keeping" } else { "cutting" };
        write!(f, "({} -> {:?} {} {}) {}", self.dice.expr(), self.dice.roll().1, op, self.to_keep, self.roll())
    }
}

#[derive(Debug, PartialEq)]
pub struct SuccessDice {
    dice: NumericDice,
    target: isize,
    keep_higher: bool,
}

impl<'a> SuccessDice {
    pub fn new(dice: NumericDice, target: isize, keep_higher: bool) -> Self {
        Self {
            dice,
            target,
            keep_higher,
        }
    }
}

impl Roll<usize> for SuccessDice {
    fn roll(&self) -> usize {
        let (_, history) = self.dice.roll();
        let count = if self.keep_higher {
            history.iter().filter(|&&v| v >= self.target).count()
        } else {
            history.iter().filter(|&&v| v <= self.target).count()
        };
        count
    }
}

impl fmt::Display for SuccessDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let op = if self.keep_higher { "at least" } else { "at most" };
        write!(f, "({} -> {:?} {} {}) {}", self.dice.expr(), self.dice.roll().1, op, self.target, self.roll())
    }
}

#[derive(Debug, PartialEq)]
pub struct FateDice {
    total: isize,
    scalar: isize,
}

impl FateDice {
    pub fn new(total: isize, scalar: isize) -> Self {
        Self {
            total,
            scalar,
        }
    }
}

impl Roll<isize> for FateDice {
    fn roll(&self) -> isize {
        let mut value = 0;
        for _ in 0..self.total {
            value += roll::between(-1, 1);
        }
        value
    }
}

impl fmt::Display for FateDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not supported (yet)")
    }
}

#[derive(Debug, PartialEq)]
pub struct GenesysDice {}

impl fmt::Display for GenesysDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not supported (yet)")
    }
}

#[derive(Debug, PartialEq)]
pub enum MultiDice {
    Plain(PlainDice),
    Trim(TrimDice),
    Success(SuccessDice),
    Fate(FateDice),
    Genesys(GenesysDice),
}

impl fmt::Display for MultiDice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            MultiDice::Plain(dice) => write!(f, "{}", dice),
            MultiDice::Trim(dice) => write!(f, "{}", dice),
            MultiDice::Success(dice) => write!(f, "{}", dice),
            MultiDice::Fate(dice) => write!(f, "{}", dice),
            MultiDice::Genesys(dice) => write!(f, "{}", dice),
        }
    }
}
