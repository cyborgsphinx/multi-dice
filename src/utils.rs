pub fn reduce<T, F, B>(iter: &mut T, func: F) -> B
where T: Iterator<Item=B>,
      F: FnMut(B, T::Item) -> B
{
    let start = iter.next().expect("Could not reduce");
    iter.fold(start, func)
}
