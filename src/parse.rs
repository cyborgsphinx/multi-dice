use std::iter::Peekable;
use std::fmt;

use super::tokenize::{DiceTokenIter, Token};
use super::dice::{NumericDice, PlainDice, TrimDice, SuccessDice, FateDice, GenesysDice, MultiDice};

#[derive(Debug)]
enum Dice {
    Numeric(NumericDice),
    Fate(isize),
    Gen(isize),
}

#[derive(Debug)]
enum DiceOrNumber {
    Dice(NumericDice),
    Number(isize),
}

#[derive(Debug, PartialEq)]
pub enum Expected {
    PlusOrMinus,
    Number,
    Dice,
    Supported,
    Join,
    Numeric,
}

impl fmt::Display for Expected {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            Expected::PlusOrMinus => "Expected '+' or '-'",
            Expected::Number => "Expected a number",
            Expected::Dice => "Expected a dice expression of the form XdY",
            Expected::Supported => "This feature is currently unsupported",
            Expected::Join => "Expected a joining phrase ('+', '-', 'above', 'below', 'keep', 'cut')",
            Expected::Numeric => "Expected numeric dice",
        };
        write!(f, "{}", msg)
    }
}

#[derive(Debug, PartialEq)]
pub struct DiceParseError {
    expected: Expected,
    got: Option<Token>,
}

impl DiceParseError {
    pub fn new(e: Expected, tok: Option<Token>) -> Self {
        Self { expected: e, got: tok }
    }
}

impl fmt::Display for DiceParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(tok) = &self.got {
            write!(f, "{}, got {:?}", self.expected, tok)
        } else {
            write!(f, "{}, got nothing", self.expected)
        }
    }
}

pub fn dice(input: &str) -> Result<MultiDice, DiceParseError> {
    let mut tokens = DiceTokenIter::new(input).peekable();
    get_base(&mut tokens)
}

fn is_trim(tok: &Token) -> bool {
    matches!(tok, Token::Keep | Token::Cut)
}

fn is_success(tok: &Token) -> bool {
    matches!(tok, Token::Above | Token::Below)
}

fn is_add(tok: &Token) -> bool {
    matches!(tok, Token::Plus | Token::Minus)
}

fn finalize_trim(mut tokens: &mut Peekable<DiceTokenIter>, dice: NumericDice, _modifier: isize, keep_higher: bool) -> Result<MultiDice, DiceParseError> {
    let num = get_num(&mut tokens)?;
    Ok(MultiDice::Trim(TrimDice::new(dice, num, keep_higher)))
}

fn finalize_success(mut tokens: &mut Peekable<DiceTokenIter>, dice: NumericDice, higher: bool) -> Result<MultiDice, DiceParseError> {
    let num = get_num(&mut tokens)?;
    Ok(MultiDice::Success(SuccessDice::new(dice, num, higher)))
}

fn finalize_plain(mut tokens: &mut Peekable<DiceTokenIter>, mut dice: PlainDice) -> Result<MultiDice, DiceParseError> {
    while let Some(tok) = tokens.next() {
        if !is_add(&tok) {
            return Err(DiceParseError::new(Expected::PlusOrMinus, Some(tok)));
        }
        let positive = tok == Token::Plus;
        match get_dice_or_number(&mut tokens)? {
            DiceOrNumber::Dice(d) => {
                if positive {
                    dice.add_dice(d);
                } else {
                    dice.add_dice(d.negate());
                }
            },
            DiceOrNumber::Number(n) => {
                if positive {
                    dice.update_scalar(n);
                } else {
                    dice.update_scalar(-n);
                }
            },
        }
    }
    Ok(MultiDice::Plain(dice))
}

fn get_base(mut tokens: &mut Peekable<DiceTokenIter>) -> Result<MultiDice, DiceParseError> {
    match get_dice(&mut tokens)? {
        Dice::Numeric(dice_expr) => { // could be plain, trim, or success
            match tokens.next() {
                Some(trim) if is_trim(&trim) => {
                    finalize_trim(&mut tokens, dice_expr, 0, trim == Token::Keep)
                },
                Some(success) if is_success(&success) => {
                    finalize_success(&mut tokens, dice_expr, success == Token::Above)
                },
                Some(add) if is_add(&add) => {
                    let positive = add == Token::Plus;
                    match get_dice_or_number(&mut tokens)? {
                        DiceOrNumber::Dice(d) => {
                            let plain = if positive {
                                PlainDice::with_dice(vec![dice_expr, d])
                            } else {
                                PlainDice::with_dice(vec![dice_expr, d.negate()])
                            };
                            finalize_plain(&mut tokens, plain)
                        },
                        DiceOrNumber::Number(n) => {
                            let plain = if positive {
                                PlainDice::new(vec![dice_expr], n)
                            } else {
                                PlainDice::new(vec![dice_expr], -n)
                            };
                            finalize_plain(&mut tokens, plain)
                        },
                    }
                },
                Some(tok) => Err(DiceParseError::new(Expected::Join, Some(tok))),
                None => Ok(MultiDice::Plain(PlainDice::with_dice(vec![dice_expr]))),
            }
        },
        Dice::Fate(n) => {
            let scalar = match tokens.next() {
                None => 0,
                Some(tok) if is_add(&tok) => get_num(&mut tokens)?,
                tok => return Err(DiceParseError::new(Expected::Number, tok)),
            };
            Ok(MultiDice::Fate(FateDice::new(n, scalar)))
        },
        // TODO: Genesys Support
        Dice::Gen(..) => {
            Err(DiceParseError::new(Expected::Supported, tokens.next()))
        },
    }
}

fn is_genesys(tok: &Token) -> bool {
    matches!(tok, Token::Ability | Token::Proficiency | Token::Boost | Token::Difficulty | Token::Challenge | Token::Setback)
}

fn get_dice_or_number(mut tokens: &mut Peekable<DiceTokenIter>) -> Result<DiceOrNumber, DiceParseError> {
    match tokens.peek() {
        Some(Token::Number(..)) => {
            let num = get_num(&mut tokens)?;
            Ok(DiceOrNumber::Number(num))
        },
        Some(Token::Total(..)) => {
            match get_dice(&mut tokens)? {
                Dice::Numeric(dice) => Ok(DiceOrNumber::Dice(dice)),
                _ => Err(DiceParseError::new(Expected::Numeric, None)),
            }
        },
        _ => Err(DiceParseError::new(Expected::Number, tokens.next()))
    }
}

fn get_dice(tokens: &mut Peekable<DiceTokenIter>) -> Result<Dice, DiceParseError> {
    match tokens.next() {
        Some(Token::Total(total_str)) => {
            let total = total_str.parse().map_err(|_| DiceParseError::new(Expected::Number, tokens.next()))?;
            match tokens.next() {
                Some(Token::Number(sides_str)) => {
                    let sides = sides_str.parse().map_err(|_| DiceParseError::new(Expected::Number, tokens.next()))?;
                    Ok(Dice::Numeric(NumericDice::new(total, sides)))
                },
                Some(Token::Fate) => Ok(Dice::Fate(total)),
                // TODO: Genesys Support
                Some(tok) if is_genesys(&tok) => Err(DiceParseError::new(Expected::Supported, tokens.next())),
                tok => Err(DiceParseError::new(Expected::Dice, tok)),
            }
        },
        tok => Err(DiceParseError::new(Expected::Dice, tok)),
    }
}

fn get_num(tokens: &mut Peekable<DiceTokenIter>) -> Result<isize, DiceParseError> {
    match tokens.next() {
        Some(Token::Number(num)) => {
            num.parse().map_err(|_| DiceParseError::new(Expected::Number, Some(Token::Number(num))))
        },
        tok => Err(DiceParseError::new(Expected::Number, tok)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parser_can_parse_plain_expression() {
        let input = "2d4 + 6";
        let res = dice(input);
        let dice = PlainDice::new(vec![NumericDice::new(2, 4)], 6);
        assert_eq!(res, Ok(MultiDice::Plain(dice)));
    }

    #[test]
    fn parser_can_parse_single_expression() {
        let input = "1d4";
        let res = dice(input);
        let dice = PlainDice::with_dice(vec![NumericDice::new(1, 4)]);
        assert_eq!(res, Ok(MultiDice::Plain(dice)));
    }

    #[test]
    fn parser_can_add_dice() {
        let input = "1d20 + 1d4 + 5";
        let res = dice(input);
        let dice = PlainDice::new(vec![NumericDice::new(1, 20), NumericDice::new(1, 4)], 5);
        assert_eq!(res, Ok(MultiDice::Plain(dice)));
    }

    #[test]
    fn parser_can_parse_keep_expression() {
        let input = "4d6 keep 3";
        let res = dice(input);
        let dice = TrimDice::new(NumericDice::new(4, 6), 3, true);
        assert_eq!(res, Ok(MultiDice::Trim(dice)));
    }

    // Uncomment when trim supports modifiers
    //#[test]
    //fn parser_can_parse_keep_expression_with_add() {
    //    let input = "2d20 + 5 keep 1";
    //    let res = dice(input);
    //    let dice = TrimDice::new(NumericDice::new(2, 20), 1, true);
    //    assert_eq!(res, Ok(MultiDice::Trim(dice)));
    //}

    #[test]
    fn parser_can_parse_success_expression() {
        let input = "1d10 above 5";
        let res = dice(input);
        let dice = SuccessDice::new(NumericDice::new(1, 10), 5, true);
        assert_eq!(res, Ok(MultiDice::Success(dice)));
    }

    #[test]
    fn parser_can_parse_fate_expression() {
        let input = "4dF + 3";
        let res = dice(input);
        assert_eq!(res, Ok(MultiDice::Fate(FateDice::new(4, 3))));
    }

    // Uncomment when Genesys is supported
    //#[test]
    //fn parser_can_parse_genesys_expression() {
    //    let input = "3dA + 2dP + 4dB + 2dD + 1dC + 3dS";
    //    let res = dice(input);
    //    assert_eq!(res, Ok(MultiDice::Genesys(GenesysDice)));
    //}
}
