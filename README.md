Multi-Dice
==========

Multi-Dice is a discord bot for rolling dice.
It can do plain numeric dice, keep/drop calculations, success counting, as well as Fate/Fudge and Genesys systems.

Plain Dice Rolling
------------------

The kind of expression you're most used to:

    roll! 1d20 + 5

Keep/Drop Dice
--------------

This is for when you want to keep the highest or lowest total of an expression (e.g. for advantage in 5e)

    roll! 2d20 + 2 keep 1

Successes
---------

This is for when you want to count the number of totals above or below a target number

    roll! 4d10 above 5

Fate/Fudge
----------

For using the dice specific to the Fate system

    roll! 3dF + 2

Genesys
-------

For using the Genesys proprietary dice system

    roll! 3dA + 1dP + 3dB + 2dD + 1dC + 1dS

Note: This use is under construction

Roll Again
----------

You can also roll your most recent roll again

    roll!again

Running the Bot
---------------

To run multi-dice on a unix-based server, run the following command

    $ DISCORD_TOKEN=<token> cargo run

Windows not supported, but if you can make it work good job